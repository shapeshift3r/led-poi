#include <Adafruit_NeoPixel.h>

#define PINLED 2
#define BUTTONPIN 1

// Primary colour definitions
enum Color {UNDEF,RED,ORANGE,YELLOW,LIGHT_GREEN,GREEN,GREENY_BLUE,CYAN,LIGHT_BLUE,BLUE,PURPLE,PINK,MAGENTA};
// number of modes we have defined
int num_modes = 8;
// initial mode to load, (@TODO) unless a mode is saved in EEPROM
volatile int mode = 0;
volatile unsigned long last_interrupt_time = 0;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(24, PINLED, NEO_GRB + NEO_KHZ800);

void setup(){
    Serial.begin(38400);
    
    strip.begin();
    strip.show(); // Initialize all pixels to 'off'
    
    pinMode(BUTTONPIN, INPUT); // sets the digital pin as output<
    attachInterrupt(BUTTONPIN, isrService, FALLING); // interrupt 1 is data ready
    
    randomSeed(672198);
    
    delay(2000);
    Serial.println("---------------------------------------");
}

void loop() {
    Serial.println("Main Loop");
    Serial.println(mode);
    
    switch (mode) {
        case 0:
            colour(0, strip.Color(0, 0, 0));
            break;
        case 1:
            {
                int colours[] = {BLUE,PURPLE,PINK,MAGENTA};
                stripedTriangle(1, colours, sizeof(colours), 3, false);
            }
            break;
        case 2:
            {
                int colours[] = {RED,ORANGE,YELLOW,LIGHT_GREEN,GREEN,GREENY_BLUE,CYAN,LIGHT_BLUE,BLUE,PURPLE,PINK,MAGENTA};
                stripedTriangle(2, colours, sizeof(colours), 3, true);
            }
            break;
        case 3:
            redRainbow(3, 5);
            break;
        case 4:
	        rainbowFlash2(4, 20, 20);
	        break;
        case 5:
	        rainbowFlash2(5, 30, 10);
	        break;
        case 6:
            rainbowFlash2(6, 50, 20);
            break;
        case 7:
            rainbow2(7,5);
            break;
        case 8:
            rainbowCycle2(8,5);
            break;
        default:
            break;
    }
}

void isrService()
{
    unsigned long interrupt_time = millis();
    
    // If interrupts come faster than 200ms, assume it's a bounce and ignore
    if ( interrupt_time - last_interrupt_time > 200 )
    {
        Serial.println("isrService");
        
        if( mode >= num_modes )
            mode = 0;
        else
            mode++;
        
        Serial.print("new mode: ");
        Serial.println(mode);
    }
    
    last_interrupt_time = interrupt_time;
}

void colour(int myMode, uint32_t c) {
    Serial.println("colour");
    
    for( uint16_t i=0; i < strip.numPixels(); i++ ) {
        strip.setPixelColor(i, c);
    }
    strip.show();
    
    while(1) {
        if( mode != myMode )
            return;
    }
}

void colourFlash(int myMode, uint32_t c, uint8_t wait) {
    Serial.println("colourFlash");
    while(1) {
        if( mode != myMode )
            return;
        
        for( uint16_t i=0; i < strip.numPixels(); i++ ) {
            strip.setPixelColor(i, c);
            Serial.print('.');
        }
        Serial.println();
        strip.show();
        delay(wait);
        
        if( mode != myMode )
            return;
        
        for( uint16_t i=0; i < strip.numPixels(); i++ ) {
            strip.setPixelColor(i, 0);
            Serial.print('.');
        }
        Serial.println();
        strip.show();
        delay(wait);
    }
}

void rainbow(int myMode, int wait) {
    Serial.println("rainbow");
    uint16_t i, j;
    while(1) {
        for(j=0; j<256; j=j+4) {
            for( i=0; i < strip.numPixels(); i++ ) {
                strip.setPixelColor(i, Wheel((j) & 255));
            }
            strip.show();
            delay(wait);
            
            if( mode != myMode )
                return;
        }
    }
}

void redRainbow(int myMode, int wait) {
    Serial.println("redRainbow");
    uint16_t i, j;
    while(1) {
        for(j=30; j<90; j++) {
            for( i=0; i < strip.numPixels(); i++ ) {
                strip.setPixelColor(i, Wheel((j) & 255));
            }
            strip.show();
            delay(wait);
            
            if( mode != myMode )
                return;
        }
        for(j=90; j>30; j--) {
            for( i=0; i < strip.numPixels(); i++ ) {
                strip.setPixelColor(i, Wheel((j) & 255));
            }
            strip.show();
            delay(wait);
            
            if( mode != myMode )
                return;
        }
    }
}

void yellowAndGreen(int myMode, int wait) {
    Serial.println("yellowAndGreen");
    uint16_t i, j;
    while(1) {
        for(j=0; j<256; j=j+4) {
            for( i=0; i < strip.numPixels(); i++ ) {
                strip.setPixelColor(i, Wheel((j) & 45));
            }
            strip.show();
            delay(wait);
            
            if( mode != myMode )
                return;
        }
    }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
    if(WheelPos < 85) {
        return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
    } else if(WheelPos < 170) {
        WheelPos -= 85;
        return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
    } else {
        WheelPos -= 170;
        return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
    }
}


void rainbowFlash(int myMode, uint8_t wait) {
    Serial.println("rainbowFlash");
    uint16_t i, j;
    
    while(1) {
        for(j=0; j<256; j = j + 4) {
            for( i=0; i < strip.numPixels(); i++ ) {
                strip.setPixelColor(i, Wheel((j) & 255));
            }
            strip.show();
            delay(wait);
            
            if( mode != myMode )
            return;
            
            for( i=0; i < strip.numPixels(); i++ ) {
                strip.setPixelColor(i, 0);
            }
            
            strip.show();
            delay(wait);
            
            if( mode != myMode )
            return;
            
        }
    }
}

void rainbowFlash2(int myMode, uint8_t on, uint8_t off) {
    Serial.println("rainbowFlash");
    uint16_t i, j;
    
    while(1) {
        for(j=0; j<256; j = j + 4) {
            for( i=0; i < strip.numPixels(); i++ ) {
                strip.setPixelColor(i, Wheel((j) & 255));
            }
            strip.show();
            delay(on);
            
            if( mode != myMode )
            return;
            
            for( i=0; i < strip.numPixels(); i++ ) {
                strip.setPixelColor(i, 0);
            }
            
            strip.show();
            delay(off);
            
            if( mode != myMode )
            return;
            
        }
    }
}
void rainbow2(int myMode, uint8_t wait) {
    uint16_t i, j;
    
    for(j=0; j<256; j++) {
        for(i=0; i<strip.numPixels(); i++) {
            strip.setPixelColor(i, Wheel((i+j) & 255));
        }
        strip.show();
        delay(wait);
        if( mode != myMode )
        return;
    }
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle2(int myMode, uint8_t wait) {
    uint16_t i, j;
    
    for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
        for(i=0; i< strip.numPixels(); i++) {
            strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + j) & 255));
        }
        strip.show();
        delay(wait);
        if( mode != myMode )
        return;
    }
}

/*
 * Returns a colour by name
 * Valid names,
 *
 * UNDEF,RED,ORANGE,YELLOW,LIGHT_GREEN,GREEN,
 * GREENY_BLUE,CYAN,LIGHT_BLUE,BLUE,PURPLE,
 * PINK,MAGENTA
 * 
 * else returns off
 */
uint32_t getColour(int colour) {
    switch (colour) {
        case RED:
            return strip.Color(255, 0, 0);
            break;
        case ORANGE:
            return strip.Color(255, 128, 0);
            break;
        case YELLOW:
            return strip.Color(255, 255, 0);
            break;
        case LIGHT_GREEN:
            return strip.Color(128, 255, 0);
            break;
        case GREEN:
            return strip.Color(0, 255, 0);
            break;
        case GREENY_BLUE:
            return strip.Color(0, 255, 128);
            break;
        case CYAN:
            return strip.Color(0, 255, 255);
            break;
        case LIGHT_BLUE:
            return strip.Color(0, 128, 255);
            break;
        case BLUE:
            return strip.Color(0, 0, 255);
            break;
        case PURPLE:
            return strip.Color(128, 0, 255);
            break;
        case PINK:
            return strip.Color(255, 0, 255);
            break;
        case MAGENTA:
            return strip.Color(255, 0, 128);
            break;
        default:
            return strip.Color(0, 0, 0);
            break;
    }
}

void triangle(int myMode, uint32_t c) {
    Serial.println("triangle");
    // need even number of pixels to work correctly
    int numPixels = strip.numPixels();
    // @TODO cache these?
    int colours[] = {BLUE,PURPLE,PINK,MAGENTA};
    int numColours = sizeof(colours);
    int currentColour = BLUE;
    // Store LED off colour
    uint32_t off = getColour(UNDEF);
    // Reverse lighting direction
    int rev = 0;
    // First led to light
    int first = ( numPixels / 2 ) - 1;
    // Last led to light
    int last = numPixels / 2;
        
    while(1) {
        for( uint16_t i=0; i < numPixels; i++  ) {
            if( i >= first && i <= last ) {
                strip.setPixelColor(i, getColour(currentColour));
            } else {
                strip.setPixelColor(i, off);
            }
        }
        strip.show();
        
        if( first == 0) {
          rev = 1;
        } else if ( first == ( numPixels / 2 ) - 1 ) {
          // starting new triangle 
          rev = 0;
          // switch the colour up
          currentColour = ( currentColour + 1 ) % numColours;
        }
        
        if(rev == 0) {
          first--;
          last++;
        } else {
          first++;
          last--;
        }
        
        if( mode != myMode )
            return;
    }
}

void stripedTriangle(int myMode, int colours[], int numColours, int width, boolean randomColours) {
    Serial.println("stripedTriangle");
    // need even number of pixels to work correctly
    int numPixels = strip.numPixels();
    // first colour
    int currentColour = 0;
    // Store LED off colour
    uint32_t off = getColour(UNDEF);
    // Reverse lighting direction
    int rev = 0;
    // First led to light
    int first = ( numPixels / 2 ) - 1;
    // Last led to light
    int last = numPixels / 2;
        
    while(1) {
        for( uint16_t i=0; i < numPixels; i++  ) {
            if( i >= first && i <= last ) {
                strip.setPixelColor(i, getColour(currentColour));
            } else {
                strip.setPixelColor(i, off);
            }
        }
        strip.show();
        
        if( first == 0) {
          rev = 1;
        } else if ( first == ( numPixels / 2 ) - 1 ) {
          // starting new triangle 
          rev = 0;
          // switch the colour up
        }
        
        if( first % width == 0 ) {
          if( randomColours ) {
            currentColour = (int) random(0, numColours - 1);
          } else {
            currentColour = ( currentColour + 1 ) % numColours;
          }
        }
        
        if(rev == 0) {
          first--;
          last++;
        } else {
          first++;
          last--;
        }
        
        if( mode != myMode )
            return;
    }
}
